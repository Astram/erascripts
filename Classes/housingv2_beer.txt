function onCreated() {
  this.furniture_id = 49;
  this.image = "glass-beer1.gif";
  this.join("housingv2_furniture");
}

//#CLIENTSIDE
function onActionGrab(){
  setAni("era_beerdrink","");
  player.chat = "*gulp*";
  player.client.stonertime += int(random(1, 2));
  sleep(1);
  player.chat = "";
}
