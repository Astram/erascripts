function onCreated() {
  this.furniture_id = 1;
  this.furniture_options = {
    {"Update Text",
      {
        {"string", "Please enter the text for the block:"}
      }
    },
    {"Toggle Unstick", null},
    {"Clone Staff Block",
      {
        {"string", "Enter the account of the block owner:"}
      }
    }
  };
  
  this.unstick = false;
  
  this.customblocks = { // Please do not add .gif animations. These are staffblocks and not filmlets.
                        "blackfires",       "era_staffblock-blackboynew.png",
                        "lusterlx1",        "era_block_lusty.png",
                        "afsgrrggg",        "era_staffblock-dest.png",
                        "Irrashinal",       "era_cloydsoulblock.gif",
                        "PunkRules",        "era_block_Katbot2.png",
                        "Delta",            "era_staffblock-delta.png",
                        "bizzam",           "era_block-bizzam.png",
                        "Bloodwork",        "deb-block.png",
                        "pandapower",       "era_panda-newsb.png",
                        "Kenshi no HeiWa",  "era_falblock.png",
                        "Requiem",          "era_staffblock-requiem.png",
                        "Sequel",           "era_staffblock-sequel.png",
                        "sethq77",          "era_staffblock-sethq77.png",
                        "Fiberwyre_P2P",    "era_block-fiberwire.gif",
                        "Name",             "zeyo-block-name.png",
                        "LuxGraal",         "era_staffblock-lux.png",
                        "Xloria",           "staffblock-xloria.png",
                        "ClevIzBak",        "era_staffblock-clev.png",
                        "MonsterMan",       "era_block-mm.png",
                        "Green142",         "era_staffblock-green.png",
                        "Trak",             "era_trakblock.png",
                        "Nonepwnz",         "era_staffblock-madz.png",
                        "PK_Inc",           "era_staffblock-pika.png",
                        "Best_Player",      "era_clyde-staffblock.png",
                        "Zelus",            "era_block_zelus.png",
                        "BrentWood",        "era_brent_block.png",
                        "Raio_P2P",         "era_staffblock-raio.png",
                        "HonchoP2P",        "era_staffblock-honcho.png",
                        "xXballaXx",        "era_block_creed.png",
                        "xDarkTAx",         "era_staffblock-ty.png",
                        "BillyWild",        "era_block_billywild-mercy.png",
                        "GodGaea",          "era_staffblock-john.png",
                        "berh5242",         "era_block-fab.png",
                        "Levvlii",          "era_lev-staffblock.gif",
                        "TripleE",          "era_block-tripleE.png",
                        "NethKrizzU",       "era_block-neth.png",
                        "ibra07",           "era_block-ibra.png",
                        "death_zephlyn",    "era_block_death.png",
                        "amf101",           "era_block-amf.png",
                        "Konshus",          "era_konshus-staffblock.png" ,
                        "Graal2360162",     "era_goat_block.png",
                        "sabdoy",           "block.png",
                        "Gagums",           "era_staffblock_gagums.png",
                        "Amandabae",        "era_staffblock-amanda.png",
                        "Timothy2000",      "era_valithor_staffblock.png",
                        "AuelZz",           "era_haydawg_staffblock.png",
                        "Graal3457388",     "era_block-timo.png",
                        "Graal4041726",     "astram_block.png",
                        "Joshualx",     "era_joshual-block.png",
                        "RandomH3ro",       "era_LT-staffblock.png",
                        "Zenno",      "era_zenno-staffblock1.png",
                        "BrutaL",      "era_brutal-block.png",
                        "Graal3058696",      "era_t9staffblock.png",
                        "MonkeyBob",         "era_zachblock.png",
                        "P_2_4",      "era_p-block.png",
                        "SaetarShadowFlare", "era_block-saetar.png"
  };
  
  this.image = "block.png";
  this.join("housingv2_furniture");
}

function onOptionUpdate_Text(data, account) {
  this.chat = data[0];
}

function onOptionToggle_Unstick(acc) {
  this.unstick = !this.unstick;
  if (this.unstick)
    sendMessage("Unstick mode turned on!");
  else
    sendMessage("Unstick mode turned off!");
}

function onOptionClone_Staff_Block(data, acc) {
  for (temp.i = 0; i < this.customblocks.size(); i += 2) {
    if (this.customblocks[i] == data[0]) {
      this.image = this.customblocks[i + 1];
      sendMessage("Staff block cloned!");
      return;
    }
  }
  this.image = "block.png";
  sendMessage("Could not find the staff member to clone!");
}

function onActionGrab() {
  if (this.unstick)
    player.unstick();
}
