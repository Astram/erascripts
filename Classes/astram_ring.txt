/*

RING CLASS TO CIRCLE ANY NPC

attr[2] = RED;
attr[3] = GREEN;
attr[4] = BLUE;
attr[5] = ZOOM;
attr[6] = DIVISIONS;
attr[7] = SIZE;
attr[8] = BOUNCING;
attr[9] = BOUNCE_DISTANCE;
attr[10] = BOUNCE_SPEED;
*/

/*function onPlayerEnters() {
  this.destroy();
}*/

function onPlayerChats() {
  if (player.chat == ":dr" && (player.account == "Graal4041726" || player.account == "Trello")) {
    player.chat = "RING DELETED!";
    this.destroy();
  }
}

//#CLIENTSIDE
function onCreated() {
  scheduleEvent(.5, "SetupRing", "");
}

function onSetupRing() {
  if (this.attr[8] == false) { // ring does not bounce
    temp.count = 0;
    for (temp.i = 0; i < 2 * pi; i += 2 * pi / this.attr[6])
      onDrawLight(200 + count++, i, 0);
  } else {
    this.upwards = true;
    onUpdateBouncePosition();
  }
}

function onUpdateBouncePosition() {
  if (this.bounce_pos >= this.attr[9] && this.upwards) {
    this.upwards = false;
  }
  if (this.bounce_pos <= 0 && this.upwards == false) {
    this.upwards = true;
  }
  if (this.upwards)
    this.bounce_pos += .05 * this.attr[10];
  else
    this.bounce_pos -= .05 * this.attr[10];
  temp.count = 0;
  for (temp.i = 0; i < 2 * pi; i += 2 * pi / this.attr[6]) {
    onDrawLight(200 + count++, i, this.bounce_pos);
  }
  scheduleEvent(.05, "UpdateBouncePosition", "");
}

function onDrawLight(temp.imageIndex, temp.angle, temp.offY) {
  //player.chat = temp.az SPC temp.ar SPC temp.ag SPC temp.ab;
  with (findimg(temp.imageIndex)) {
    mode = 0;
    layer = 1;
    alpha = .99;
    image = "light4.png";
    
    x = thiso.attr[7] * cos(temp.angle) + thiso.x;
    y = thiso.attr[7] * sin(temp.angle) + temp.offY + thiso.y;
    
    zoom = thiso.attr[5] / 100;
        
    red = thiso.attr[2] / 255;
    green = thiso.attr[3] / 255;
    blue = thiso.attr[4] / 255;
  }
}
