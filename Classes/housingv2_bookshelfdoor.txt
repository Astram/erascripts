function onCreated() {
  this.furniture_id = 3;
  this.image = "era_mercy-bookcase3c.png";
  this.join("housingv2_furniture");
  this.join("housingv2_restrict");
  this.join("housingv2_connection");
  this.canConnectTo = true;
}

function onUpdatePosition() {
  this.start_x = this.x;
}

function onActionGrab() {
  if (!canUse(player.account))
    return;
  if (isConnected())
    return;
  setTimer(.05);
  this.opening = true;
}

function onRecieveSignal(acc) {
  setTimer(.05);
  this.opening = true;
}

function onTimeOut() {
  if (this.opening) {
    this.x += .1;
    if (this.x < this.start_x + 5) {
      setTimer(.05);
    } else {
      this.opening = false;
      setTimer(3);
    }
  } else {
    this.x -= .1;
    if (this.x > this.start_x) {
      setTimer(.05);
    }
  }
}
