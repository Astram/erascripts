function onCreated() {
  this.furniture_id = 19;
  this.furniture_options = {
    {"Toggle State", null}
  };
  setImgPart("dargaro-fridge.gif", 64, 0, 40, 100);
  this.join("housingv2_furniture");
  this.open = false;
  this.attr[6] = 2;
  this.attr[7] = 5;
}

function onActionGrab() {
  if (!this.open) {
    this.x -= 1;
    setImgPart("dargaro-fridge.gif", 0, 0, 50, 100);
    this.open = true;
  } else {
    this.x += 1;
    setImgPart("dargaro-fridge.gif", 64, 0, 40, 100);
    this.open = false;
  }
}

function onOptionToggle_State() {
  onActionGrab();
}
