
function onCreated() {
  this.setShape(1, 32, 32);
}

function onPlayerChats() {
  if (player.chat == "pay tax") {
    if (this.tax < 0) {
      echo("[Item Flip]: " @ player.account @ " tried to spawn money using a glitch!");
      this.tax = 0;
      player.unstick();
      return;
    }
    if (player.x in |10.875, 29.5| && player.y in |3.75, 12.8125| && this.x < 29.5) {
      if (payTax(player))
        triggerAction(this.x + .5, this.y + .5, "DisplayTaxes", this.tax, true);
    }
    if (player.x in |30.3125, 50.3125| && player.y in |3.75, 12.8125| && this.x > 29.5) {
      if (payTax(player))
        triggerAction(this.x + .5, this.y + .5, "DisplayTaxes", this.tax, true);
    }
  }
  if (player.clientr.staff < 2) return;
  else if (player.chat == ":resettax") {
    this.paid = false;
    triggerAction(this.x + .5, this.y + .5, "ResetTaxes", null);
  } else if (player.chat.starts(":tax ")) {
    this.paid = false;
    temp.amount_to_tax = int(player.chat.substring(5));
    this.tax = int(amount_to_tax * .03);
    triggerAction(this.x + .5, this.y + .5, "DisplayTaxes", this.tax, false);
  }
}

function payTax(pl) {
  if (this.paid) return false;
  if (pl.rupees >= this.tax) {
    pl.rupees -= this.tax;
    pl.addMessage("I have paid the taxes!", "b", 1);
    serverr.itemflip_tax += this.tax;
    this.paid = true;
    return true;
  } else {
    pl.addMessage("I cannot afford taxes!", "b", 3);
    return false;
  }
}

//#CLIENTSIDE
function onCreated() {
  this.setShape(1, 32, 32);
}

function onActionResetTaxes() {
  hideimgs(200, 201);
}

function onActionDisplayTaxes(tax, paid) {
  with (findimg(200)) {
    layer = 2;
    text = "Tax: $" @ tax;
    x = thiso.x;
    y = thiso.y;
    font = "Tahoma";
    style = "bc";
    zoom = .65;
    if (!paid) {
      red = 1;
      blue = green = 0;
    } else {
      green = 1;
      red = blue = 0;
    }
  }
}
