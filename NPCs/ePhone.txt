function onCreated() {
  registerApps();
  loadApps();
  //echo();
}

function registerApps() {
  //       {"NICE NAME", "IMAGE", "NAME", PRICE, DESCRIPTION, DEFAULT};
  this.0 = {"null", "1", "hel2i", 0, "Call in a personal helicopter", true};
  this.1 = {"Safe House", "era_tank-ephone-appicon3.png", "info", 0, "Information about the ePhone", true};
  this.2 = {"Search", "era_tank-ephone-appicon1.png", "info", 0, "Information about the ePhone", true};
  this.3 = {"Information", "era_tank-ephone-appicon5.png", "info", 0, "Information about the ePhone", true};
  this.4 = {"Fly", "era_tank-ephone-appicon2.png", "heli", 0, "Call in a personal helicopter", true};
  this.5 = {"Contacts", "era_tank-ephone-appicon4.png", "contacts", 0, "All your friends with an ePhone", true};
}

function loadApps() {
  temp.apps = 0;
  this.apps = 0;
  for (temp.v : this.getVarNames()) {
    if (isNumber(v)) {
      apps++;
      this.apps.add(v);
      //echo("[EPHONE]: Registered app " @ this.( @ v)[0] @ " with id " @ v @ "!");
    }
  }
  echo("[EPHONE]: There were " @ apps @ " apps registered!");
}

function isNumber(temp.number) {
  return (number.starts("1") || number.starts("2") || number.starts("3") || number.starts("4") || number.starts("5") || number.starts("6") || number.starts("7") || number.starts("8") || number.starts("9") || number.starts("0"));
}

public function getAppName(temp.id) {
  return getApp(id)[0];
}

public function getAppPrice(temp.id) {
  return getApp(id)[1];
}

public function getAppImage(temp.id) {
  return getApp(id)[2];
}

public function getAppRealName(temp.id) {
  return getApp(id)[3];
}

public function getAppDescription(temp.id) {
  return getApp(id)[4];
}

public function getAppDefault(temp.id) {
  return getApp(id)[5];
}

public function getApp(temp.id) {
  for (temp.app : this.apps) {
    if (app == id)
      return this.( @ id);
  }
}

function getDefaultApps() {
  temp.apps = {};
  for (temp.app : this.apps) {
    if (getAppDefault(app))
      apps.add(app);
  }
  return apps;
}

public function getApps(temp.account) {
  temp.apps = this.( @ "Apps_" @ temp.account);
  for (temp.app : getDefaultApps())
    temp.apps.add(app);
  return temp.apps;
}


public function addApp(temp.account, temp.app) {
  this.( @ "Apps_" @ account).add(app);
}

public function removeApp(temp.account, temp.app) {
  this.( @ "Apps_" @ account).remove(app);
}

public function hasApp(temp.account, temp.app) {
  return app in this.( @ "Apps_" @ account);
}
