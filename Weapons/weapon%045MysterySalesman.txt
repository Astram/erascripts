GRAWP001
REALNAME -MysterySalesman
IMAGE bcalarmclock.png
SCRIPT
function onActionServerSide() {
  if (params[0] == "purchase") {
    temp.npc = findNPC("MysteryShop");
    temp.quantity = npc.( @ "item" @ params[1]).quantity;
    temp.amount = npc.( @ "item" @ params[1]).amount;
    if (quantity == null || quantity == 0)
      quantity = 1;
    temp.price = npc.( @ "item" @ params[1]).price;
    if (hasPurchased(player)) {
      player.addMessage("You have already made a purchase!", "b", 3);
      return;
    }
    if (amount <= 0) {
      player.addMessage("This item is sold out!", "b", 3);
      return;
    }
    if (player.rupees >= price) {
      player.rupees -= price;
      player.addItem(npc.( @ "item" @ params[1]).id, quantity);
      npc.( @ "item" @ params[1]).amount--;
      temp.inc = npc.( @ "item" @ params[1]).inc;
      if (inc != null && inc != 0)
        npc.( @ "item" @ params[1]).price *= inc;
      onCompletePurchase(player);
    } else {
      player.addMessage("You cannot afford this item!", "b", 3);
    }
  }
}

function hasPurchased(pl) {
  temp.npc = findNPC("MysteryShop");
  //pl.chat = npc.active_loc;
  for (temp.n : findlevel(npc.active_loc[0]).npcs) {
    if (n.isinclass("mysteryshop"))
      return n.hasUsed(pl.account);
  }
  return true;
}

function onCompletePurchase(pl) {
  temp.npc = findNPC("MysteryShop");
  temp.npcs = findlevel(npc.active_loc[0]).npcs;
  for (temp.i = 0; i < npcs.size(); i ++) {
    if (npcs[i].isinclass("mysteryshop")) {
      npcs[i].addUsed(pl.account);
      break;
    }
  }
  pl.addMessage("You have successfully purchased the item!", "b", 1);
}

public function onDisplayShop(pl) {
  this.items = findNPC("MysteryShop").getItems();
  this.itemdata = {};
  for (temp.i = 0; i < this.items.size(); i ++)
    this.itemdata.add(findNPC("DB_Items").( @ this.items[i][0]));
  pl.triggerClient("gui", this.name, "display", this.items, this.itemdata);
}

//#CLIENTSIDE
function onActionClientSide() {
  if (params[0] == "display")
    onDrawShop(params[1], params[2]);
}

function onDrawShop(items, itemdata) { 
  
  //player.chat = "Items: " @ itemdata;
  
  MysteryShop_Window.destroy();
  
  new GuiWindowCtrl("MysteryShop_Window") {
    profile = CrowWindow2;
    width = 300;
    height = 425;
    x = GraalControl.width / 2 - width / 2;
    y = GraalControl.height / 2 - height / 2;
    canminimize = canmaximize = canresize = canmove = false;
  
    new GuiShowImgCtrl("MysteryShop_Head") {
      x = 20;
      y = 40;
      image = "era_head-blackhand.gif";
      party = 64;
      partw = 32;
      parth = 32;
      zoom = 1;
    }
    
    new GuiMLTextCtrl("MysteryShop_Briefing") {
      useownprofile = true;
      x = 60;
      y = 20;
      width = 220;
      height = 150;
      profile.fontcolor = {255, 255, 255};
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      profile.fontsize = 12;
      text = "I see you've found me. Shhhh. Don't tell anyone. I have stolen a few high-quality items from various stores around Era. I'm trying to get my hands off of them. Be warned though, my prices are higher since this is illegal business we're doing.";
    }
    
    new GuiTextCtrl("MysteryShop_Item1") {
      useownprofile = true;
      width = 200;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 14;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      text = items[0][3] == null ? itemdata[0][1] : items[0][3];
      x = MysteryShop_Window.width / 2 - getTextWidth(14 / 20, "b", "Tahoma", text) / 2;
      y = 130;
    }
    
    new GuiTextCtrl("MysteryShop_Item1_Amount") {
      useownprofile = true;
      width = 200;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 9;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      if (items[0][2] > 0)
        text = "Only " @ items[0][2] @ " left!";
      else
        text = "All sold out!";
      x = MysteryShop_Window.width / 2 - getTextWidth(9 / 18, "b", "Tahoma", text) / 2;
      y = 145;
    }
    
    new GuiShowImgCtrl("MysteryShop_Item1_Icon") {
      image = itemdata[0][2];
      x = 40;
      y = 165;
      partw = parth = 48;
    }
    
    new GuiTextCtrl("MysteryShop_Item1_Price") {
      useownprofile = true;
      width = 200;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 12;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      text = "My Price: $" @ items[0][1];
      x = 92;
      y = 160;
    }
    
    new GuiButtonCtrl("MysteryShop_Item1_Purchase") {
      profile = NewsEditorDarkButtonProfile;
      width = 150;
      height = 20;
      text = "Purchase";
      x = 92;
      y = 180;
    }
    
    new GuiTextCtrl("MysteryShop_Item2") {
      useownprofile = true;
      width = 200;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 14;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      text = items[1][3] == null ? itemdata[1][1] : items[1][3];
      x = MysteryShop_Window.width / 2 - getTextWidth(14 / 20, "b", "Tahoma", text) / 2;
      y = 220;
    }
    
    new GuiTextCtrl("MysteryShop_Item2_Amount") {
      useownprofile = true;
      width = 200;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 9;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      if (items[1][2] > 0)
        text = "Only " @ items[1][2] @ " left!";
      else
        text = "All sold out!";
      x = MysteryShop_Window.width / 2 - getTextWidth(9 / 18, "b", "Tahoma", text) / 2;
      y = 235;
    }
    
    new GuiShowImgCtrl("MysteryShop_Item2_Icon") {
      image = itemdata[1][2];
      x = 40;
      y = 255;
      partw = parth = 48;
    }
    
    new GuiTextCtrl("MysteryShop_Item2_Price") {
      useownprofile = true;
      width = 200;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 12;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      text = "My Price: $" @ items[1][1];
      x = 92;
      y = 250;
    }
    
    new GuiButtonCtrl("MysteryShop_Item2_Purchase") {
      profile = NewsEditorDarkButtonProfile;
      width = 150;
      height = 20;
      text = "Purchase";
      x = 92;
      y = 270;
    }
    
    new GuiTextCtrl("MysteryShop_Item3") {
      useownprofile = true;
      width = 200;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 14;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      text = items[2][3] == null ? itemdata[2][1] : items[2][3];
      x = MysteryShop_Window.width / 2 - getTextWidth(14 / 20, "b", "Tahoma", text) / 2;
      y = 310;
    }
    
    new GuiTextCtrl("MysteryShop_Item3_Amount") {
      useownprofile = true;
      width = 200;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 9;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      if (items[2][2] > 0)
        text = "Only " @ items[2][2] @ " left!";
      else
        text = "All sold out!";
      x = MysteryShop_Window.width / 2 - getTextWidth(9 / 18, "b", "Tahoma", text) / 2;
      y = 325;
    }
    
    new GuiShowImgCtrl("MysteryShop_Item3_Icon") {
      image = itemdata[2][2];
      x = 40;
      y = 345;
      partw = parth = 48;
    }
    
    new GuiTextCtrl("MysteryShop_Item3_Price") {
      useownprofile = true;
      width = 200;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 12;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      text = "My Price: $" @ items[2][1];
      x = 92;
      y = 340;
    }
    
    new GuiButtonCtrl("MysteryShop_Item3_Purchase") {
      profile = NewsEditorDarkButtonProfile;
      width = 150;
      height = 20;
      text = "Purchase";
      x = 92;
      y = 360;
    }
    
    new GuiTextCtrl("MysteryShop_Disclaimer") {
      useownprofile = true;
      width = 400;
      height = 20;
      profile.fontcolor = {255, 255, 255};
      profile.fontsize = 13;
      profile.font = "Tahoma";
      profile.fontstyle = "b";
      text = "You can only buy one item! Choose wisely.";
      x = MysteryShop_Window.width / 2 - getTextWidth(13 / 22, "b", "Tahoma", text) / 2;
      y = 390;
    }
    
  }
  
}

function MysteryShop_Item1_Purchase.onAction() {
  handlePurchase(1);
}

function MysteryShop_Item2_Purchase.onAction() {
  handlePurchase(2);
}

function MysteryShop_Item3_Purchase.onAction() {
  handlePurchase(3);
}

function handlePurchase(id) {
  MysteryShop_Window.destroy();
  triggerServer("gui", this.name, "purchase", id);
}
SCRIPTEND
